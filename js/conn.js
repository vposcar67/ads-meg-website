import { initializeApp } from "https://www.gstatic.com/firebasejs/10.4.0/firebase-app.js";
import { getAuth } from "https://www.gstatic.com/firebasejs/10.4.0/firebase-auth.js";
import { getDatabase } from "https://www.gstatic.com/firebasejs/10.4.0/firebase-database.js";
import { getStorage, ref, uploadBytes, getDownloadURL} from "https://www.gstatic.com/firebasejs/10.4.0/firebase-storage.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyAmbhdNj_10SZaa-IqCoITWmtBtvsctTQo",
    authDomain: "adsmeg-1b320.firebaseapp.com",
    projectId: "adsmeg-1b320",
    storageBucket: "adsmeg-1b320.appspot.com",
    messagingSenderId: "992973933352",
    appId: "1:992973933352:web:6c8f068f8abd385138de1d"
  };

  // Initialize Firebase
  export const app = initializeApp(firebaseConfig);
  export const auth = getAuth(app);
  export const db = getDatabase(app);
  export const gSt = getStorage(app);
  
  const storageRef = ref(gSt);
  export function upload(file,fileName){
    const childRef = ref(storageRef,'img/'+fileName)
    uploadBytes(childRef,file).then((snapshot) => console.log('uploaded'))
    return childRef; 
  }