import {auth} from './conn.js';
import {signInWithEmailAndPassword} from "https://www.gstatic.com/firebasejs/10.4.0/firebase-auth.js";
const loginform = document.getElementById('login-form');

loginform.addEventListener('submit', async (e) => {
    e.preventDefault();
    const email = loginform['correo'].value;
    const pass = loginform['contra'].value;
    try{
        const credentials = await signInWithEmailAndPassword(auth,email,pass);
        console.log(credentials);
        window.location.href = '/html/backstage.html';
    } catch (error){
        console.log(error);
        alert("Correo o contraseña incorrectos")
    }
})
